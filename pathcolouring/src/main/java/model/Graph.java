package main.java.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Provides abstraction of graph
 */
public class Graph {
    /** Represents vertices of the graph, implemented as HashMap of Integer representing label and Vertex itself to ease the way of finding specific vertex */
    private Map<Integer, Vertex> vertices = new HashMap<>();

    /** Method that adds new vertex
     *
     * @param label - label of currently added vertex
     * @param vertex - vertex object being added itself
     * @throws RuntimeException - in case of existing vertex addition attempt
     */
    public void addVertex(Integer label, Vertex vertex) {
        if (!vertices.containsKey(label)) {
            vertices.put(label, vertex);
        } else {
            throw new RuntimeException("Can't add the vertex because vertex with same label already exists!");
        }

    }

    /** Getter for vertices Map
     * @return vertices Map
     */
    public Map<Integer, Vertex> getVertices() {
        return vertices;
    }

    /** Creates deep copy of the graph
     * @return deep copy of graph the method is called on
     */
    public Graph deepCopy() {
        Graph graphCopy =  new Graph();

        for(Map.Entry<Integer, Vertex> entry : vertices.entrySet()) {
            graphCopy.addVertex(entry.getKey(), entry.getValue().deepCopy());
        }

        graphCopy.updateNeighbours();
        return graphCopy;
    }

    private void updateNeighbours() {
        for(Map.Entry<Integer, Vertex> entry : vertices.entrySet()) {
            List<Integer> neighbourLabelsToAdd = new ArrayList<>();
            for (Map.Entry<Vertex, Double> neighbour : entry.getValue().getNeighbours().entrySet()) {
                neighbourLabelsToAdd.add(neighbour.getKey().getLabel());
            }
            entry.getValue().getNeighbours().clear();
            for(Integer label : neighbourLabelsToAdd) {
                entry.getValue().addNeighbour(vertices.get(label));
            }
        }
    }
}
