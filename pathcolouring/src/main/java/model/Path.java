package main.java.model;

/** Represents path for each call to be coloured
 */
public class Path {
    /** Label of start point for the path */
    private int startLabel;
    /** Label of end point for the path */
    private int endLabel;

    /** Constructs the Path object
     * @param startLabel - label of start point
     * @param endLabel - label of end point
     */
    public Path(int startLabel, int endLabel) {
        this.startLabel = startLabel;
        this.endLabel = endLabel;
    }

    /** Getter for label of start point
     * @return label of start point
     */
    public int getStartLabel() {
        return startLabel;
    }

    /** Getter for label of end point
     * @return label of end point
     */
    public int getEndLabel() {
        return endLabel;
    }

    /** Compares two Path objects, considers these to be equal if both labels are the same in two paths, regardless of start and end point order
     * @param o - object to compare to
     * @return true if objects are the same, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path path = (Path) o;

        return !(endLabel != path.endLabel && endLabel != path.startLabel) && !(startLabel != path.startLabel && startLabel != path.endLabel);
    }
}
