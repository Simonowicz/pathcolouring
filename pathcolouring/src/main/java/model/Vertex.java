package main.java.model;

import java.util.HashMap;
import java.util.Map;

/** Represents vertex of the graph
 */
public class Vertex {
    /** Label of the vertex, also used to store colour of given vertex when it is being used to represent a single path found in the solution **/
    private int label;
    /** Contains adjacent vertices with amount of pheromone left on edges between them **/
    private Map<Vertex, Double> neighbours = new HashMap<>();

    /** Constructs new instance of vertex object
     * @param label label of vertex that is being created
     */
    public Vertex(int label) {
        this.label = label;
    }

    /** Method allowing addition of neighbour to the existing vertex
     * @param neighbour neighbour to add to map of adjacent vertices
     * @throws RuntimeException if provided neighbour is already present in the map
     */
    public void addNeighbour(Vertex neighbour) {
        if (!neighbours.containsKey(neighbour)) {
            neighbours.put(neighbour, 1.0);
        } else {
            throw new RuntimeException("Can't add the neighbour because it already exists!");
        }
    }

    /** Getter for neighbours map
     * @return neighbours map
     */
    public Map<Vertex, Double> getNeighbours() {
        return neighbours;
    }

    /** Getter for label of the vertex
     * @return label of the vertex
     */
    public int getLabel() {
        return label;
    }

    /** Setter for label of the vertex
     * @param label label of the vertex to be set
     */
    public void setLabel(int label) {
        this.label = label;
    }

    /** Creates deep copy of vertex with the exception of its neighbours as new neighbour memory addresses might not be known at that point
     * The method should not be called outside of context of the graph due to inability to create deep copy of neighbours
     * Neighbours should be update immediately after deep copies of vertices themselves had been created
     * @return deep copy of vertex on which the method is called
     */
    Vertex deepCopy() {
        Vertex vertexCopy = new Vertex(this.label);
        for(Map.Entry<Vertex, Double> entry : neighbours.entrySet())  {
            vertexCopy.addNeighbour(entry.getKey());
        }

        return vertexCopy;
    }
}
