package main.java.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Represents abstraction for found solution
 */
public class Solution {
    /** Contains the order of vertices selected in given path and colour assigned to it */
    private Map<List<Integer>, String> pathLabelsWithColours = new HashMap<>();
    /** Specifies number of colours needed in the solution to cover all the calls */
    private int coloursNeeded;

    /** Compares quality of two solutions
     * @param otherSolution - solution to compare against the object method is called on
     * @return true if this object needs less colours or if requires same amount of colours, but sum of path length is shorter
     */
    public boolean isBetterThan(Solution otherSolution) {
        return this.coloursNeeded < otherSolution.coloursNeeded || this.coloursNeeded == otherSolution.coloursNeeded && this.sumPathLength() < otherSolution.sumPathLength();
    }

    /** Sums length of all the paths used in the solution
     * @return length of all the paths
     */
    public int sumPathLength() {
        int pathLength = 0;
        for (Map.Entry<List<Integer>, String> entry : pathLabelsWithColours.entrySet()) {
            pathLength += entry.getKey().size();
        }
        return pathLength;
    }

    /** Getter for object containing order of vertices within a path and colour assigned to that path
     * @return order of vertices and label of colour assigned to the path
     */
    public Map<List<Integer>, String> getPathLabelsWithColours() {
        return pathLabelsWithColours;
    }

    /** Getter for colours needed
     * @return number of colours needed for the solution
     */
    public int getColoursNeeded() {
        return coloursNeeded;
    }

    /** Setter for colours needed
     * @param coloursNeeded - new number of colours needed for the solution
     */
    public void setColoursNeeded(int coloursNeeded) {
        this.coloursNeeded = coloursNeeded;
    }

    /** Allows easy representation of solution object to human readable format
     * @return human readable String representing solution structure
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Colours needed: ").append(coloursNeeded).append("\n");

        for(Map.Entry<List<Integer>, String> entry : pathLabelsWithColours.entrySet()) {
            stringBuilder.append("Path: ");
            for (Integer integer : entry.getKey()) {
                stringBuilder.append(integer).append(" ");
            }
            stringBuilder.append("colour: ").append(entry.getValue()).append("\n");
        }

        return stringBuilder.toString();
    }
}
