package main.java;

import main.java.model.Graph;
import main.java.model.Path;
import main.java.model.Solution;
import main.java.model.Vertex;
import main.java.pathcolourfinder.PathColourFinder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;

/** EntryPoint for path colouring algorithm
 */
public class EntryPoint {
    /** input graph **/
    private static Graph GRAPH = new Graph();
    /** input paths **/
    private static List<Path> PATHS = new ArrayList<>();
    /** helper attribute, used to store not fully initialized vertices from the input file **/
    private static Map<Integer, Vertex> NON_COMPLETE_VERTICES = new HashMap<>();
    /** default number of maximum iterations **/
    private static int DEFAULT_MAX_ITERATIONS = 10000;

    /** main function, methods needed in order to get the solution are called from here
     * @param args input parameters for the program
     */
    public static void main(String [] args) {
        if (args.length < 1) {
            System.err.println("Correct usage EntryPoint <pathToTheFile> (maxIterations)");
        } else {
            Integer maxIterationsFromArgs = null;
            if (args.length > 1) {
                maxIterationsFromArgs = Integer.parseInt(args[1]);
            }
            readFileAndInitializeGraph(args[0]);
            PathColourFinder pathColourFinder = new PathColourFinder(GRAPH, PATHS, maxIterationsFromArgs != null ? maxIterationsFromArgs : DEFAULT_MAX_ITERATIONS);
            try {
                Solution bestSolution = pathColourFinder.findPathColours();
                System.out.println("Found the solution:\n" + bestSolution.toString());
                System.exit(1);
            } catch (BrokenBarrierException | InterruptedException e) {
                System.err.println("Thread runner encountered unexpected exception, message was: " + e.getMessage());
            }
        }
    }

    /** Method that reads input file and creates the graph
     * @param fileName path to the file
     */
    private static void readFileAndInitializeGraph(String fileName) {
        try {
            int mode = 0;

            System.out.println("Reading file: " + fileName);
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = bufferedReader.readLine();
            while (line != null) {
                if (line.equals("")) {
                    mode = 1;
                    line = bufferedReader.readLine();
                    if (NON_COMPLETE_VERTICES.size() > 0) {
                        throw new Exception("Graph from the file is not valid, not all vertices were initialized!");
                    }
                    continue;
                }
                parseLine(line, mode);
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.err.println("Error occurred when reading the file, exception: " + e.getMessage());
            System.exit(0);
        }
    }

    /** Parses each line of input file
     * @param line file line
     * @param mode mode of parsing (determines whether given line determines new vertex or path)
     */
    private static void parseLine(String line, int mode) {
        if (mode == 0) {
            parseGraphLine(line);
        } else {
            parsePathLine(line);
        }
    }

    /** Parses line of the file representing new vertex
     * @param line line from the file
     */
    private static void parseGraphLine(String line) {
        String [] vertices = line.split(" ");
        Integer label = Integer.parseInt(vertices[0]);
        Vertex vertex;
        if (!NON_COMPLETE_VERTICES.containsKey(label)) {
            vertex = new Vertex(label);
        } else {
            vertex = NON_COMPLETE_VERTICES.get(label);
        }
        for (int i = 1; i < vertices.length; i++) {
            Integer neighbourLabel = Integer.parseInt(vertices[i]);
            if (GRAPH.getVertices().containsKey(neighbourLabel)) {
                vertex.addNeighbour(GRAPH.getVertices().get(neighbourLabel));
            } else if (NON_COMPLETE_VERTICES.containsKey(neighbourLabel)) {
                vertex.addNeighbour(NON_COMPLETE_VERTICES.get(neighbourLabel));
            } else {
                Vertex neighbourVertex = new Vertex(neighbourLabel);
                NON_COMPLETE_VERTICES.put(neighbourLabel, neighbourVertex);
                vertex.addNeighbour(neighbourVertex);
            }
        }

        if (NON_COMPLETE_VERTICES.containsKey(label)) {
            NON_COMPLETE_VERTICES.remove(label);
        }

        GRAPH.addVertex(label, vertex);
    }

    /** Parses line of the file representing new path
     * @param line line from the file
     */
    private static void parsePathLine(String line) {
        String [] pathVerticesLabels = line.split(" ");
        Path currentPath = new Path(Integer.parseInt(pathVerticesLabels[0]), Integer.parseInt(pathVerticesLabels[1]));
        if (!PATHS.contains(currentPath)) {
            PATHS.add(currentPath);
        }
    }
}
