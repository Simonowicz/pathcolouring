package main.java.pathcolourfinder;

import main.java.model.Graph;
import main.java.model.Path;
import main.java.model.Vertex;

import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Semaphore;

/** Represents a single ant worker that's supposed to find path between two vertices in a graph, supports multi threading, each worker finds a single path in a graph
 */
public class AntWorkerThread extends Thread {
    /** Graph in which the path should be found **/
    private Graph graph;
    /** Path that should be found **/
    private Path pathToFind;
    /** Order of vertices in a graph that realize passage from start point of the path to its end point **/
    private List<Integer> pathFound;
    /** Reference to pathColourFinder object, needed in order to synchronize ant worker threads **/
    private PathColourFinder pathColourFinder;
    /** Object enabling synchronization of threads **/
    private Semaphore semaphore = new Semaphore(1);

    /** evaporation rate of pheromone on an edge **/
    private static final double EVAPORATION_RATE = 0.9;

    /** Constructor for ant worker
     * @param graph graph in which path should be found
     * @param path path to be found
     * @param pathColourFinder reference to pathColourFinder object, needed for synchronization of threads
     */
    public AntWorkerThread(Graph graph, Path path, PathColourFinder pathColourFinder) {
        this.graph = graph;
        this.pathToFind = path;
        this.pathColourFinder = pathColourFinder;
    }

    /** Starts a single thread that attempts to find path in the graph
     */
    @Override
    public void run() {
        while (true) {
            try {
                // wait until cyclic barrier is reinitialized
                semaphore.acquire();
                pathFound = findPath();
                pathColourFinder.getCyclicBarrier().await();

                updateGraph();
            } catch (InterruptedException e) {
                // do nothing, PathColourFinder class interrupts the thread once the solution is found
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (NoNeighboursLeftException e) {
                System.err.println("One of the paths could not be found in the graph");
                System.exit(0);
            }
        }
    }

    /** Attempts to find path in the graph
     * @return order of vertices of passage through vertices that realizes the path
     */
    private List<Integer> findPath() {
        Stack<Integer> labelStack = new Stack<>();
        labelStack.push(pathToFind.getStartLabel());
        depthFirstSearch(labelStack, new ArrayList<Integer>() { { add(pathToFind.getStartLabel()); } });
        return labelStack;
    }

    /** Recursive method that works in a similar way to DFS algorithm that realizes path finding process, takes into account heuristic based on amount of pheromone on edge that's left
     * @param labelStack stack structure representing order of vertices that realise the path
     * @param visited list of vertices that had already been visited and should not be taken into account any more, makes sure there are no loops in the path found
     */
    private void depthFirstSearch(Stack<Integer> labelStack, List<Integer> visited) {
        Integer currentVertexLabel = labelStack.peek();

        for (int i = 0; i < graph.getVertices().get(currentVertexLabel).getNeighbours().size(); i++) {
            if (labelStack.peek() == pathToFind.getEndLabel()) {
                return;
            }
            Integer nextVertexLabel;
            try {
                nextVertexLabel = getNextVertexLabel(currentVertexLabel, visited);
            } catch (NoNeighboursLeftException e) {
                labelStack.pop();
                return;
            }
            visited.add(nextVertexLabel);
            labelStack.push(nextVertexLabel);
            depthFirstSearch(labelStack, visited);
        }

        if (labelStack.peek() != pathToFind.getEndLabel()) {
            throw new NoNeighboursLeftException();
        }
    }

    /** Method implementing behaviour of a single ant that chooses next vertex to go to, based on roulette wheel selection method
     * @param currentVertexLabel current position of the ant
     * @param visited list of already visited vertices
     * @return vertex to go to
     */
    private Integer getNextVertexLabel(Integer currentVertexLabel, List<Integer> visited) {
        Random rng = new Random();

        Map<Vertex, Double> validNeighbours = new HashMap<>();
        for (int i = 0; i < graph.getVertices().get(currentVertexLabel).getNeighbours().size(); i++) {
            for (Map.Entry<Vertex, Double> entry : graph.getVertices().get(currentVertexLabel).getNeighbours().entrySet()) {
                if (!visited.contains(entry.getKey().getLabel())) {
                    validNeighbours.put(entry.getKey(), entry.getValue());
                }
            }
        }

        if (validNeighbours.size() == 0) {
            throw new NoNeighboursLeftException();
        }

        double [] cumulativeScores = new double[validNeighbours.size()];
        Collection<Double> doubleValues = validNeighbours.values();
        Iterator<Double> doubleValuesIterator = doubleValues.iterator();
        cumulativeScores[0] = doubleValuesIterator.next();
        int i = 1;
        while (doubleValuesIterator.hasNext()) {
            cumulativeScores[i] = doubleValuesIterator.next() + cumulativeScores[i - 1];
            i++;
        }

        double randomScore = rng.nextDouble() * cumulativeScores[cumulativeScores.length - 1];
        int index = Arrays.binarySearch(cumulativeScores, randomScore);

        if (index < 0) {
            index = Math.abs(index + 1);
        }

        Iterator<Map.Entry<Vertex, Double>> neighboursIterator = validNeighbours.entrySet().iterator();
        for (int iteratorCount = 0 ; iteratorCount < index; iteratorCount++) {
            neighboursIterator.next();
        }

        return neighboursIterator.next().getKey().getLabel();
    }

    /** Method implementing pheromone addition and evaporation
     */
    private void updateGraph() {
        double pheromoneToAdd = 1.0 / (double) pathColourFinder.getRecentSolution().getColoursNeeded();
        addPheromonesOnPathFound(pheromoneToAdd);
        evaporatePheromones();
    }

    /** Method responsible for pheromone addition to graph edges
     * @param pheromoneToAdd amount of pheromone to add to the edge
     */
    private void addPheromonesOnPathFound(double pheromoneToAdd) {
        for (int i = 0 ; i < pathFound.size() - 1 ; i++) {
            Vertex currentVertex = graph.getVertices().get(pathFound.get(i));
            for (Map.Entry<Vertex, Double> neighbour : currentVertex.getNeighbours().entrySet()) {
                if (pathFound.get(i + 1).equals(neighbour.getKey().getLabel())) {
                    currentVertex.getNeighbours().put(neighbour.getKey(), currentVertex.getNeighbours().get(neighbour.getKey()) + pheromoneToAdd);
                }
            }
        }
    }

    /** Method responsible for pheromone evaporation
     */
    private void evaporatePheromones() {
        for (Map.Entry<Integer, Vertex> entry : graph.getVertices().entrySet()) {
            for (Map.Entry<Vertex, Double> neighbour : entry.getValue().getNeighbours().entrySet()) {
                entry.getValue().getNeighbours().put(neighbour.getKey(), entry.getValue().getNeighbours().get(neighbour.getKey()) * EVAPORATION_RATE);
            }
        }
    }

    /** Getter for path found
     * @return path found
     */
    public List<Integer> getPathFound() {
        return pathFound;
    }

    /** Getter for path that ant worker is supposed to find
     * @return path that ant worker is supposed to find
     */
    public Path getPathToFind() {
        return pathToFind;
    }

    /** Object that's used for thread synchronization
     * @return semaphore that forbids threads to run before given solution is graded
     */
    public Semaphore getSemaphore() {
        return semaphore;
    }
}
