package main.java.pathcolourfinder;

import main.java.model.Graph;
import main.java.model.Path;
import main.java.model.Solution;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/** Class that controls the process of path colour finding
 */
public class PathColourFinder {
    /** Graph in which paths need to be coloured **/
    private Graph graph;
    /** List of paths to be coloured in the graph **/
    private List<Path> paths;
    /** Maximum number of iterations that the algorithm will perform **/
    private final int maxIterations;
    /** List of ant worker threads searching for paths (one worker per path is created) **/
    private List<AntWorkerThread> antWorkers = new ArrayList<>();
    /** Cyclic barrier used to synchronize the process **/
    private CyclicBarrier cyclicBarrier;
    /** Current best solution found **/
    private Solution bestSolution;
    /** Solution found in most recent iteration **/
    private Solution recentSolution;
    /** Reference to the class responsible for grading solutions **/
    private SolutionGrader solutionGrader = new SolutionGrader();

    /** Constructor for PathColourFinder object
     * @param graph graph in which paths need to be coloured
     * @param paths paths to be coloured
     * @param maxIterations maximum number of iterations to be performed
     */
    public PathColourFinder(Graph graph, List<Path> paths, int maxIterations) {
        this.graph = graph;
        this.paths = paths;
        this.maxIterations = maxIterations;

        initializeAntWorkers();
    }

    /** Method creating ant worker threads, deep copy of the graph is passed to each worker
     */
    private void initializeAntWorkers() {
        for (Path path : paths) {
            antWorkers.add(new AntWorkerThread(graph.deepCopy(), path, this));
        }
    }

    /** Method controlling process of path colouring
     * @return best solution found within maximum number of iterations specified
     * @throws BrokenBarrierException
     * @throws InterruptedException
     */
    public Solution findPathColours() throws BrokenBarrierException, InterruptedException {
        initializeCyclicBarrierAndStartWorkers();
        cyclicBarrier.await();

        for (int i = 1; i < maxIterations; i++) {
            cyclicBarrier.reset();
            unlockAntWorkers();
            cyclicBarrier.await();
        }

        for  (AntWorkerThread antWorker : antWorkers) {
            antWorker.interrupt();
        }

        return bestSolution;
    }

    /** Method initializing cyclic barrier used for process synchronization and starting workers that search for the paths
     */
    private void initializeCyclicBarrierAndStartWorkers() {
        cyclicBarrier = initializeCyclicBarrier();
        for (AntWorkerThread antWorkerThread : antWorkers) {
            antWorkerThread.start();
        }
    }

    /** Method initializing cyclic barrier
     * @return cyclic barrier controlling process synchronization
     */
    private CyclicBarrier initializeCyclicBarrier() {
        cyclicBarrier = new CyclicBarrier(paths.size() + 1, () -> {
            recentSolution = solutionGrader.gradeSolution(antWorkers);
            if (bestSolution == null) {
                bestSolution = recentSolution;
            } else {
                if (recentSolution.isBetterThan(bestSolution)) {
                    bestSolution = recentSolution;
                }
            }
        });

        return cyclicBarrier;
    }

    /** Method that unlocks all ant workers after solution is graded
     */
    private void unlockAntWorkers() {
        for (AntWorkerThread antWorkerThread :  antWorkers) {
            antWorkerThread.getSemaphore().release();
        }
    }

    /** Getter of cyclic barrier, used in ant workers to synchronize the process
     * @return currently used cyclic barrier
     */
    public CyclicBarrier getCyclicBarrier() {
        return cyclicBarrier;
    }

    /** Getter for most recent solution
     * @return most recent solution
     */
    public Solution getRecentSolution() {
        return recentSolution;
    }
}
