package main.java.pathcolourfinder;

/** Exception indicating that there are no more neighbours left to choose from
 */
public class NoNeighboursLeftException extends RuntimeException {
    public NoNeighboursLeftException() {
        super();
    }
}
