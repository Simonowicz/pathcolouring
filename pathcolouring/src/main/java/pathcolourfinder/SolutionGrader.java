package main.java.pathcolourfinder;

import main.java.model.Path;
import main.java.model.Solution;
import main.java.model.Vertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Method that grades solutions found in a single iteration of the application
 */
public class SolutionGrader {
    /** Constructor of the solution grader object
     * @param antWorkers list of ant workers being used
     * @return abstraction of graded solution, candidate for the best one
     */
    public Solution gradeSolution(List<AntWorkerThread> antWorkers) {
        Map<Path, Vertex> collisionGraph = buildCollisionGraph(antWorkers);
        colourCollisionGraph(collisionGraph);
        return createSolution(collisionGraph, antWorkers);
    }

    /** Method that is responsible for building collision graph in order to grade the solution
     * @param antWorkers list of ant workers being used
     * @return collision graph for the paths found
     */
    private Map<Path, Vertex> buildCollisionGraph(List<AntWorkerThread> antWorkers) {
        Map<Path, Vertex> collisionGraph = new HashMap<>();

        for (AntWorkerThread antWorker : antWorkers) {
            collisionGraph.put(antWorker.getPathToFind(), new Vertex(0)); // labels will later on be used to define colours of the vertex
        }

        for (int i = 0; i < antWorkers.size(); i++) {
            for (int j = i + 1; j < antWorkers.size(); j++) {
                if (isTwoPathsColliding(antWorkers.get(i).getPathFound(), antWorkers.get(j).getPathFound())) { // two paths collide, create and edge between them
                    collisionGraph.get(antWorkers.get(i).getPathToFind()).addNeighbour(collisionGraph.get(antWorkers.get(j).getPathToFind()));
                    collisionGraph.get(antWorkers.get(j).getPathToFind()).addNeighbour(collisionGraph.get(antWorkers.get(i).getPathToFind()));
                }
            }
        }

        return collisionGraph;
    }

    /** Method determining if the two paths are colliding
     * @param firstPath first path
     * @param secondPath second path
     * @return true if the two paths are colliding with each other, false otherwise
     */
    private boolean isTwoPathsColliding(List<Integer> firstPath, List<Integer> secondPath) {
        for (Integer label : firstPath) {
            if (secondPath.contains(label)) {
                return true;
            }
        }

        return false;
    }

    /** Vertex colouring algorithm (RLF algorithm implemented), state of collision graph is modified after the method is completed
     * @param collisionGraph collision graph to be coloured
     */
    private void colourCollisionGraph(Map<Path, Vertex> collisionGraph) {
        int uncoloured = collisionGraph.size();
        int colourNumber = 0;
        while (uncoloured > 0) {
            Vertex maxDegreeVertex = findUncolouredMaxDegreeVertex(collisionGraph);
            colourNumber++;
            maxDegreeVertex.setLabel(colourNumber);
            uncoloured--;
            List<Vertex> nonNeighbours = findUncolouredNonNeighbours(collisionGraph, colourNumber);
            while (nonNeighbours.size() > 0) {
                Vertex suitableVertex = findSuitableVertex(collisionGraph, nonNeighbours, colourNumber);
                suitableVertex.setLabel(colourNumber);
                uncoloured--;
                nonNeighbours = findUncolouredNonNeighbours(collisionGraph, colourNumber);
            }
        }
    }

    /** Helper function for vertex colouring algorithm, finds uncoloured vertex of maximum degree
     * @param collisionGraph collision graph to be coloured
     * @return uncoloured vertex of maximum degree
     */
    private Vertex findUncolouredMaxDegreeVertex(Map<Path, Vertex> collisionGraph) {
        Vertex maxDegreeVertex = null;
        for (Map.Entry<Path, Vertex> entry : collisionGraph.entrySet()) {
            if (maxDegreeVertex == null && entry.getValue().getLabel() == 0) {
                maxDegreeVertex = entry.getValue();
            } else {
                if (entry.getValue().getLabel() == 0 && maxDegreeVertex.getNeighbours().size() < entry.getValue().getNeighbours().size()) {
                    maxDegreeVertex = entry.getValue();
                }
            }
        }

        return maxDegreeVertex;
    }

    /** Helper function for vertex colouring algorithm, finds uncoloured non-neighbour vertices to vertices of given colour
     * @param collisionGraph collision graph to be coloured
     * @param colourNumber label of the colour currently considered
     * @return list of uncoloured non-neighbour vertices to vertices of given colour
     */
    private List<Vertex> findUncolouredNonNeighbours(Map<Path, Vertex> collisionGraph, int colourNumber) {
        List<Vertex> uncolouredNonNeighbours =  new ArrayList<>();

        for (Map.Entry<Path, Vertex> entry : collisionGraph.entrySet()) {
            if (entry.getValue().getLabel() == 0 && isVertexNonAdjacentToOtherWithGivenColour(entry.getValue(), colourNumber)) {
                uncolouredNonNeighbours.add(entry.getValue());
            }
        }

        return uncolouredNonNeighbours;
    }

    /** Helper function for vertex colouring algorithm, determines whether given vertex is adjacent to any vertex of given colour
     * @param vertex currently considered vertex
     * @param colourNumber currently considered colour label
     * @return true if considered vertex is non-adjacent to any other vertex of given colour, false otherwise
     */
    private boolean isVertexNonAdjacentToOtherWithGivenColour(Vertex vertex, int colourNumber) {
        for (Map.Entry<Vertex, Double> neighbour : vertex.getNeighbours().entrySet()) {
            if (neighbour.getKey().getLabel() == colourNumber) {
                return false;
            }
        }

        return true;
    }

    /** Helper method for vertex colouring algorithm, provides heuristic for candidate finding of the next vertex to colour
     * @param collisionGraph collision graph to be coloured
     * @param nonNeighbours list of non neighbours to any vertices of given colour from which the candidate is selected
     * @param colourNumber label of colour currently considered
     * @return vertex with most neighbours in common with vertices of currently considered colour, if there are none vertices in common, returns vertex of the most degree within nonNeighbours list
     */
    private Vertex findSuitableVertex(Map<Path, Vertex> collisionGraph, List<Vertex> nonNeighbours, int colourNumber) {
        int verticesInCommonMax = 0;
        Vertex suitableVertex = null;

        for  (Vertex vertex : nonNeighbours) {
            if (suitableVertex == null) {
                suitableVertex = vertex;
                verticesInCommonMax = getNeighboursInCommonWithNeighboursOfColourForVertex(collisionGraph, vertex, colourNumber);
            } else {
                int currentVerticesInCommon = getNeighboursInCommonWithNeighboursOfColourForVertex(collisionGraph, vertex, colourNumber);
                if (currentVerticesInCommon > verticesInCommonMax) {
                    suitableVertex = vertex;
                    verticesInCommonMax = currentVerticesInCommon;
                }
            }
        }

        if (verticesInCommonMax == 0) {
            suitableVertex = findMaxDegreeVertex(nonNeighbours);
        }

        return suitableVertex;
    }

    /** Helper method for vertex colouring algorithm, finds vertex of maximum degree within the list provided
     * @param vertexList list of vertices from which maximum degree vertex is selected
     * @return vertex of maximum degree within the list
     */
    private Vertex findMaxDegreeVertex(List<Vertex> vertexList) {
        Vertex maxDegreeVertex = null;

        for (Vertex vertex : vertexList) {
            if (maxDegreeVertex == null) {
                maxDegreeVertex = vertex;
            } else {
                if (vertex.getNeighbours().size() > maxDegreeVertex.getNeighbours().size()) {
                    maxDegreeVertex = vertex;
                }
            }
        }

        return maxDegreeVertex;
    }

    /** Helper method for vertex colouring algorithm, returns number of neighbours in common for given vertex with other vertices of given colour
     * @param collisionGraph collision graph to be coloured
     * @param vertex currently considered vertex
     * @param colourNumber label of currently considered colour
     * @return number of neighbours in common
     */
    private int getNeighboursInCommonWithNeighboursOfColourForVertex(Map<Path, Vertex> collisionGraph, Vertex vertex, int colourNumber) {
        List<Integer> verticesInCommon = new ArrayList<>();
        for (Map.Entry<Path, Vertex> entry : collisionGraph.entrySet()) {
            if (entry.getValue().equals(vertex)) {
                // do nothing
            } else if (entry.getValue().getLabel() == colourNumber) {
                Vertex colouredVertex =  entry.getValue();
                for (Map.Entry<Vertex, Double> candidateVertex : colouredVertex.getNeighbours().entrySet())  {
                    if (vertex.getNeighbours().containsKey(candidateVertex.getKey()) && !verticesInCommon.contains(candidateVertex.getKey().getLabel())) {
                        verticesInCommon.add(candidateVertex.getKey().getLabel());
                    }
                }
            }
        }

        return verticesInCommon.size();
    }

    /** Method creating abstaction of graded solution
     * @param collisionGraph coloured collision graph
     * @param antWorkers ant workers being used
     * @return abstraction of graded solution
     */
    private Solution createSolution(Map<Path, Vertex> collisionGraph,  List<AntWorkerThread> antWorkers) {
        Solution solution = new Solution();
        solution.setColoursNeeded(countColoursInCollisionGraph(collisionGraph));

        for (AntWorkerThread antWorker : antWorkers) {
            solution.getPathLabelsWithColours().put(antWorker.getPathFound(), String.valueOf(collisionGraph.get(antWorker.getPathToFind()).getLabel()));
        }

        return solution;
    }

    /** Finds number of colours used in path colouring problem
     * @param collisionGraph coloured graph
     * @return number of colour labels found in the graph
     */
    private int countColoursInCollisionGraph(Map<Path, Vertex> collisionGraph) {
        List<Integer> colourLabels = new ArrayList<>();

        for (Map.Entry<Path, Vertex> entry : collisionGraph.entrySet()) {
            if (!colourLabels.contains(entry.getValue().getLabel())) {
                colourLabels.add(entry.getValue().getLabel());
            }
        }

        return colourLabels.size();
    }
}
